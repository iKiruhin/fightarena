﻿namespace FightArena
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbFightersLeft = new System.Windows.Forms.ListBox();
			this.lbFightersRight = new System.Windows.Forms.ListBox();
			this.btnFight = new System.Windows.Forms.Button();
			this.rtbLog = new System.Windows.Forms.RichTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lbFightersLeft
			// 
			this.lbFightersLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lbFightersLeft.FormattingEnabled = true;
			this.lbFightersLeft.ItemHeight = 24;
			this.lbFightersLeft.Location = new System.Drawing.Point(12, 12);
			this.lbFightersLeft.Name = "lbFightersLeft";
			this.lbFightersLeft.Size = new System.Drawing.Size(336, 220);
			this.lbFightersLeft.TabIndex = 0;
			// 
			// lbFightersRight
			// 
			this.lbFightersRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.lbFightersRight.FormattingEnabled = true;
			this.lbFightersRight.ItemHeight = 24;
			this.lbFightersRight.Location = new System.Drawing.Point(594, 12);
			this.lbFightersRight.Name = "lbFightersRight";
			this.lbFightersRight.Size = new System.Drawing.Size(334, 220);
			this.lbFightersRight.TabIndex = 1;
			// 
			// btnFight
			// 
			this.btnFight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.btnFight.Location = new System.Drawing.Point(368, 93);
			this.btnFight.Name = "btnFight";
			this.btnFight.Size = new System.Drawing.Size(200, 76);
			this.btnFight.TabIndex = 2;
			this.btnFight.Text = "Fight";
			this.btnFight.UseVisualStyleBackColor = true;
			this.btnFight.Click += new System.EventHandler(this.btnFight_Click);
			// 
			// rtbLog
			// 
			this.rtbLog.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.rtbLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.rtbLog.Location = new System.Drawing.Point(0, 451);
			this.rtbLog.Name = "rtbLog";
			this.rtbLog.Size = new System.Drawing.Size(940, 319);
			this.rtbLog.TabIndex = 3;
			this.rtbLog.Text = "";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(12, 247);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(60, 24);
			this.label1.TabIndex = 4;
			this.label1.Text = "label1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(591, 247);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 24);
			this.label2.TabIndex = 5;
			this.label2.Text = "label2";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(940, 770);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.rtbLog);
			this.Controls.Add(this.btnFight);
			this.Controls.Add(this.lbFightersRight);
			this.Controls.Add(this.lbFightersLeft);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox lbFightersLeft;
		private System.Windows.Forms.ListBox lbFightersRight;
		private System.Windows.Forms.Button btnFight;
		private System.Windows.Forms.RichTextBox rtbLog;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
	}
}

