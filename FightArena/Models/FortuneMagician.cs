﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public class FortuneMagician:  Elf 
	{
		public FortuneMagician(string _name) : this(_name, new Sword("Меч кладенец"))
		{
		}

		public FortuneMagician(string _name, BaseWeapon _weapon) : base(_name, _weapon)
		{

		}

		public override int GetSummaryPower()
		{
			return base.GetSummaryPower() + GetRandomBonus();
		}

		private int GetRandomBonus()
		{
			Random rnd = new Random();
			return rnd.Next(0, 100);
		}
	}
}
