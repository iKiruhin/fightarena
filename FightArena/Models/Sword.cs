﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public class Sword : BaseWeapon
	{
		public Sword(string _title)
		{
			this.Title = _title;
		}
		//public Sword(int _power)
		//{
		//	this.Power = _power;
		//}
		public override int GetPower()
		{
			return 50;
		}
	}
}
