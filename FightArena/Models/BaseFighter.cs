﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public abstract class BaseFighter
	{
		static int CurrentId = 0;
		public int Id { get; private set; }
		public BaseWeapon Weapon { get; internal set; }
		public int Power { get; internal set; }
		//public int Defence { get; internal set; }
		public string Title { get; internal set; }
		public string Name { get; set; }

		public bool IsAlive { get; set; }

		public BaseFighter()
		{
			Id = CurrentId++;
		}

		public virtual int GetSummaryPower()
		{
			//return Power + Weapon.Power;
			return Power + Weapon.GetPower();
		}

		public override string ToString()
		{
			return Name + " - " + Title;
		}
	}
}
