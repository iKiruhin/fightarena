﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public abstract class BaseWeapon
	{
		public string Title { get; internal set; }
	
		//public int Power { get; internal set; }
		public abstract int GetPower();
	}
}
