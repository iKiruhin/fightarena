﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public class SelfHarmElf :  Elf 
	{
		public SelfHarmElf(string _name) : base(_name)
		{
		}

		public SelfHarmElf(string _name, BaseWeapon _weapon) : base(_name, _weapon)
		{

		}

		public override int GetSummaryPower()
		{
			return Power - Weapon.GetPower();
		}


	}
}
