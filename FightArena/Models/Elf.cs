﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FightArena.Models
{
	public class Elf : BaseFighter
	{
		public Elf(string _name)
		 : this(_name, new Sword("Меч по умолчанию"))
		{
		}

		public Elf(string _name, BaseWeapon _weapon)
		 : base()
		{
			this.Weapon = _weapon;
			Power = 30;
			//Defence = 5;
			Title = "Эльф";
			Name = _name;
		}

		public override int GetSummaryPower()
		{
			return base.GetSummaryPower();
		}

	}
}
