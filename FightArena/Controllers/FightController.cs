﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FightArena.Models;

namespace FightArena.Controllers
{
	public class FightController
	{
		internal static List<BaseFighter> GetFighters()
		{
			List<BaseFighter> result = new List<BaseFighter>();

			BaseFighter f1 = new Elf("Легалас");
			result.Add(f1);


			BaseFighter f2 = new FortuneMagician("Маг со случайной силой", new Sword("Меч короля Артура"));
			result.Add(f2);

			BaseFighter f3 = new SelfHarmElf("Маг-самовредитель");
			result.Add(f3);



			return result;
		}

		internal static BaseFighter HandleFight(BaseFighter f1, BaseFighter f2)
		{
			BaseFighter winner = null;

			if (f1.GetSummaryPower() > f2.GetSummaryPower())
			{
				winner = f1;
			}
			else
			{
				winner = f2;
			}

			return winner;
		}
	}
}
