﻿using FightArena.Controllers;
using FightArena.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FightArena
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			SetupForm();
		}

		private void SetupForm()
		{
			List<BaseFighter> fightersList = FightController.GetFighters();

			foreach (var fighter in fightersList)
			{
				lbFightersLeft.Items.Add(fighter);
				lbFightersRight.Items.Add(fighter);
			}
		}

		private void btnFight_Click(object sender, EventArgs e)
		{
			BaseFighter f1 = lbFightersLeft.SelectedItem as BaseFighter;
			BaseFighter f2 = lbFightersRight.SelectedItem as BaseFighter;
			BaseFighter fightWinner = FightController.HandleFight(f1, f2);

			rtbLog.Text += string.Format(
			@"Бой: {0} vs {1}.
			Победитель: {2}
			
			",
			f1, f2, fightWinner);
		}
	}
}
